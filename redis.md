# Overview
Using Redis as a cache backend can be simplified using the Redis Operator on OpenShift.

## Steps

1. `oc create -f scc.yaml`
2. `oc new-project redis`
3. `oc adm policy add-scc-to-group redis-enterprise-scc system:serviceaccounts:redis`
4. Install Redis Operator in `redis` namespace
5. Create new Redis Cluster:
```yaml
apiVersion: app.redislabs.com/v1
kind: RedisEnterpriseCluster
metadata:
  name: rec
  namespace: redis
spec:
  size: 1
  persistentSpec:
    enabled: true
  uiServiceType: ClusterIP
  username: demo@redislabs.com
  redisEnterpriseNodeResources:
    limits:
      cpu: 1000m
      memory: 2Gi
    requests:
      cpu: 1000m
      memory: 2Gi
  redisEnterpriseImageSpec:
    imagePullPolicy: IfNotPresent
    repository: redislabs/redis
    versionTag: 5.4.14-31.rhel7-openshift
```