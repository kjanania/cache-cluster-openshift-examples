package com.redhat.sample.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

@SpringBootApplication
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    // @Bean
    // HazelcastInstance hazelcastInstance() {
    //     // for client HazelcastInstance LocalMapStatistics will not available
    //     return HazelcastClient.newHazelcastClient();
    //     // return Hazelcast.newHazelcastInstance();
    // }

    // @Bean
    // CacheManager cacheManager() {
    //   return new HazelcastCacheManager(hazelcastInstance());
    // }
}
