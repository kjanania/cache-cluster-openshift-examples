#!/usr/bin/env bash

oc new-project cache-clustering
# oc create -f hazelcast.yaml
oc create secret generic redhat-registry --from-file=.dockerconfigjson=$HOME/.docker/config.json --type=kubernetes.io/dockerconfigjson
oc secrets link default redhat-registry --for=pull
oc import-image openjdk/openjdk-8-rhel8 --from=registry.redhat.io/openjdk/openjdk-8-rhel8 --confirm
oc new-app . --image-stream=openjdk-8-rhel8 --name=sample-service --context-dir="./sample-service"
oc set build-secret buildconfig/sample-service redhat-registry --pull
oc create -f hazelcast.yaml
oc scale deploymentconfig/sample-service --replicas=3
oc expose svc/sample-service